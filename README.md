# README. Unit Test Triangle.

#1. Описание

Релизован unit-тест для проверки метода **getRtriangle** класса **RtriangleProvider**.

Использовалась библиотека **JUnit**, которая предоставляет удобные аннотации и прочие средства для написания тестов.

Использовался класс BitInteger, для избежания переполнения int'ов, что вполне может случится, например, если в качестве x'ов взять минимальный и максимальный int, тогда их разность переполнит тип int, а если возвести эту разность в квадрат, то переполнится даже тип long.

Все выисления производятся в целых числах, что бы избежать проблем с точностью.

Алгоритм проверки:

* Проверяем что это невырожденный треугольник, тоесть его площаль больше нуля.

* Проверяем, что один из его углов прямой. Делаем это с помощью скалярного произведения.

Для того, что бы проверить работоспособность тестов, была дописана минимальная имплементация Rtrinagle. (RtriangleImp).

#2. Инструкция по сборке и запуску

$ git clone https://sievmi@bitbucket.org/sievmi/unittesttriangle.git

$ cd unittesttriangle/

$ mvn clean test