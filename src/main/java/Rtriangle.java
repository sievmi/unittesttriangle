
public interface Rtriangle {
    int getApeX1();
    int getApeY1();
    int getApeX2();
    int getApeY2();
    int getApeX3();
    int getApeY3();
}
