public class RtriangleImp implements Rtriangle{

    public RtriangleImp(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
    }

    public int getApeX1() {
        return x1;
    }

    public int getApeY1() {
        return y1;
    }

    public int getApeX2() {
        return x2;
    }

    public int getApeY2() {
        return y2;
    }

    public int getApeX3() {
        return x3;
    }

    public int getApeY3() {
        return y3;
    }

    private int x1, y1;
    private int x2, y2;
    private int x3, y3;
}
