
public final class RtriangleProvider {

    public static Rtriangle getRtriangle() {

        // rectangular triangle
        return new RtriangleImp(1, 1,1, 0, 0, 1);

        //rectangle triangle
        //return new RtriangleImp(0, 0, 5, 0, 0, 5);

        // 0 area
        //return new RtriangleImp(0, 0, 5, 0, -2, 0);

        // no rectangular triangle
        //return new RtriangleImp(0,0,5, 1,1, 3);
    }

}