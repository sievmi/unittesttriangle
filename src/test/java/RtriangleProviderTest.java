import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertTrue;

public class RtriangleProviderTest {

    BigInteger x1;
    BigInteger y1;
    BigInteger x2;
    BigInteger y2;
    BigInteger x3;
    BigInteger y3;
    Rtriangle rtriangle;

    @Before
    public void init() {
        rtriangle = RtriangleProvider.getRtriangle();
        x1 = BigInteger.valueOf(rtriangle.getApeX1());
        y1 = BigInteger.valueOf(rtriangle.getApeY1());
        x2 = BigInteger.valueOf(rtriangle.getApeX2());
        y2 = BigInteger.valueOf(rtriangle.getApeY2());
        x3 = BigInteger.valueOf(rtriangle.getApeX3());
        y3 = BigInteger.valueOf(rtriangle.getApeY3());
    }


    public BigInteger triangleAreaX2() {
        // (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1)
        return ((x2.subtract(x1)).multiply(y3.subtract(y1)))
                .subtract((y2.subtract(y1)).multiply(x3.subtract(x1)));
    }

    boolean isRightAngle(BigInteger x1, BigInteger y1, BigInteger x2,
                         BigInteger y2, BigInteger x3, BigInteger y3) {

        // (x2 - x1) * (x3 - x1) + (y2 - y1) * (y3 - y1) == 0
        return ((x2.subtract(x1)).multiply(x3.subtract(x1)))
                .add((y2.subtract(y1)).multiply(y3.subtract(y1)))
                    .equals(BigInteger.ZERO);
    }

    @Test
    public void testGetRtrinagle() {

        assertTrue(!triangleAreaX2().equals(BigInteger.ZERO));

        boolean isRightAngle1 = isRightAngle(x1, y1, x2, y2, x3, y3);
        boolean isRightAngle2 = isRightAngle(x2, y2, x1, y1, x3, y3);
        boolean isRightAngle3 = isRightAngle(x3, y3, x1, y1, x2, y2);

        assertTrue(isRightAngle1 || isRightAngle2 || isRightAngle3);

    }
}
